def task() -> list:
    temp_tuple = (0, 36.6, 100)

    def calc(item):
        return item * 9 / 5 + 32

    return list(map(calc, temp_tuple))  # TODO  вернуть список температур по Фаренгейту)


if __name__ == "__main__":
    print(task())
