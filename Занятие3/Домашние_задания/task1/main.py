import json

items = [1, 3, 5, 4, 24, 344, 232]
items1 = items.copy()
list.reverse(items1)


def print_info(write_func):
    def wrap(name, path):
        data = write_func(name, path)
        print(f"Содержимое файла {name} -- {data}")
        return data
    return wrap


def save(i, path):
    f = open(path, "w")
    json.dump(i, f)
    f.close()


@print_info
def load(filename, path):
    with open(path) as file:
        data = json.load(file)
    return data


if __name__ == "__main__":
    save(items, "text.json")
    save(items1, "text2.json")
    load("text.json", "text.json")
    load("text2.json", "text2.json")
