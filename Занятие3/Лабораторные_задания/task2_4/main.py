import json


def task():
    filename = "input.json"
    with open(filename) as f:
        data = json.load(f)

    gen_exr = map(lambda k: k["contains_improvement_appeals"], data)
    return sum(gen_exr)


if __name__ == "__main__":
    print(task())
