INPUT_FILE = "input.txt"
OUTPUT_FILE = "output.txt"


def task():
    with open(INPUT_FILE) as f1:
        with open(OUTPUT_FILE, "w") as f2:
            s = map(lambda t: str.upper(t), f1)
            f2.writelines(s)


if __name__ == "__main__":
    task()

    with open(OUTPUT_FILE) as file:
        for line in file:
            print(line, end="")
