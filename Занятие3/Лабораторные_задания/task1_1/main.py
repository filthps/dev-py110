INPUT_FILE = "input.txt"


def task() -> None:
    with open(INPUT_FILE) as file:  # TODO открыть указатель на файл
        for string in file:
            print(string.rstrip())


if __name__ == "__main__":
    task()
