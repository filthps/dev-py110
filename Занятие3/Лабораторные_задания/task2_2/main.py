import json


def task(input_filename: str, output_filename: str) -> None:
    with open("input.json") as f:
        data = json.load(f)

    with open("output.json", "w") as f1:
        json.dump(data, f1, indent=4)

if __name__ == "__main__":
    input_file = "input.json"
    output_file = "output.json"

    task(input_file, output_file)

    with open(output_file) as output_f:
        for line in output_f:
            print(line, end="")
