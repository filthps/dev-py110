import json


def task():
    filename = "input.json"
    with open(filename) as file:
        data = json.load(file)
    return max(data, key=lambda item: item["score"])


if __name__ == "__main__":
    print(task())
