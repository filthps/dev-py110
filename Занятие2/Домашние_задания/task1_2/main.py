from itertools import chain

def flatten(list_of_lists: list, ):
    return (chain(*list_of_lists))


if __name__ == "__main__":
    matrix = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ]

    for ceil in flatten(matrix):
        print(ceil)

    print(matrix)
