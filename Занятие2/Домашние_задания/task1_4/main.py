from itertools import pairwise as pairwise_itertools

def pairwise(iterable):
    for i in pairwise_itertools(iterable):
        yield i


def task():
    for pair in pairwise("ABCDEFG"):
        print(pair)


if __name__ == "__main__":
    task()
