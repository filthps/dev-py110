from string import ascii_lowercase, ascii_uppercase, digits
from time import time
from random import randint


def time_counter(f):
    def counter(n):
        t = time()
        res = f(n)
        print(f"Заняло времени: {time() - t}")
        return res
    return counter


@time_counter
def password_gen(n=8):
    i = 0
    items = (ascii_lowercase, ascii_uppercase, digits,)
    while i < n:
        iterable = items[randint(0, 2)]
        out = iterable[randint(0, len(iterable) - 1)]
        yield out
        i += 1


if __name__ == "__main__":
    lazy_password = password_gen(12328)
    print(str.join("", lazy_password))
