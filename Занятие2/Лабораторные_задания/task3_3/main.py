def header_footer(func):  # TODO написать декоратор
    def w():
        print("=" * 8)
        func()
        print("=" * 8)
    return w


@header_footer
def my_func():
    print("Hello World")


if __name__ == "__main__":
    my_func()
