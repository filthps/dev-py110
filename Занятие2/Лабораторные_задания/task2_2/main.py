from itertools import count as count_builtin


def count(start_number: float = 1, step: float = 1):
    f_gen = (i for i in count_builtin(start_number, step))
    return f_gen


if __name__ == "__main__":
    my_count = count(10, 0.5)
    for _ in range(10):
        print(next(my_count))
